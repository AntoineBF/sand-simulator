import time
import pygame
import random
import numpy as np

COLOR_GRID = (30, 30, 30)
COLOR_BG = (8, 20, 180)
COLOR_DIE_NEXT = (50, 80, 200)
COLOR_ALIVE_NEXT = (205, 205, 2)

WIDTH = 600
HEIGHT = 600

CELL_SIZE = 15

NB_CELL_X = int(WIDTH/CELL_SIZE)
NB_CELL_Y = int(HEIGHT/CELL_SIZE)

MARGE = 1

def update(screen, grid):
    updated_grid = np.zeros_like(grid)
    rows, cols = grid.shape

    for col in range(1, cols-1):
        current_row = 1
        while current_row != rows - 2:
            color = COLOR_BG
            if (grid[current_row, col] == 1 and updated_grid[current_row, col] == 0):
                # Check if the cell below is full
                if grid[current_row + 1, col] == 1:
                    # Check if the bottom left and bottom right cells are free
                    if grid[current_row + 1, col - 1] == 0 and grid[current_row + 1, col + 1] == 0:
                        # Randomly choose one of the two directions
                        if col > 1:
                            if col < cols -2:
                                direction = random.choice([-1, 1])
                            else:
                                direction = -1
                        else:
                            direction = 1
                        #direction = random.choice([-1, 1])
                        updated_grid[current_row + 1, col + direction] = 1
                    # If only the bottom left cell is free, move there
                    elif grid[current_row + 1, col - 1] == 0 and col > 1:
                        updated_grid[current_row + 1, col - 1] = 1
                    # If only the bottom right cell is free, move there
                    elif grid[current_row + 1, col + 1] == 0 and col < cols - 2:
                        updated_grid[current_row + 1, col + 1] = 1
                    else:
                        color = COLOR_ALIVE_NEXT
                        updated_grid[current_row, col] = 1
                else:
                    color = COLOR_ALIVE_NEXT
                    updated_grid[current_row, col] = 1
            elif (grid[current_row - 1, col] == 1 and grid[current_row, col] == 0):
                color = COLOR_ALIVE_NEXT
                updated_grid[current_row, col] = 1
                updated_grid[current_row - 1, col] = 0
            elif updated_grid[current_row, col] == 0:
                color = COLOR_DIE_NEXT
                updated_grid[current_row, col] = 0

            pygame.draw.rect(screen, color, (col * CELL_SIZE, current_row * CELL_SIZE, CELL_SIZE - MARGE, CELL_SIZE - MARGE))
            current_row += 1
    return updated_grid

def main():
    # Initialization
    pygame.init()
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    grid = np.zeros((NB_CELL_Y, NB_CELL_X))
    screen.fill(COLOR_GRID)
    update(screen, grid)
    pygame.display.flip() 
    pygame.display.update()
    running = True  #False
    step = False

    # Infinite loop
    while True:
        for event in pygame.event.get():
            # To exit
            if event.type == pygame.QUIT:
                pygame.quit()
                return
            # To launch / To stop the time
            elif event.type == pygame.KEYDOWN:
                # To launch / To stop the time
                if event.key == pygame.K_SPACE:
                    running = not running
                    update(screen, grid)
                # To launch the time step-by-step
                elif event.key == pygame.K_s:
                    update(screen, grid)
                    step = True
                # To clear the grid
                elif event.key == pygame.K_c:
                    grid = np.zeros((NB_CELL_Y, NB_CELL_X))
                    screen.fill(COLOR_GRID)
                    update(screen, grid)
                    #running = False
            # To create life
            elif pygame.mouse.get_pressed()[0]:   # Left click
                pos = pygame.mouse.get_pos()
                col = pos[0] // CELL_SIZE
                row = pos[1] // CELL_SIZE
                
                grid[row, col] = 1
                update(screen, grid)
            # To kill cell
            elif pygame.mouse.get_pressed()[2]:   # Right click
                pos = pygame.mouse.get_pos()
                col = pos[0] // CELL_SIZE
                row = pos[1] // CELL_SIZE
                grid[row, col] = 0
                update(screen, grid)
            
            pygame.display.update()

        if running:
            grid = update (screen, grid)
            pygame.display.update()
            time.sleep(0.01)
        elif step:
            grid = update (screen, grid)
            pygame.display.update()
            step = False
            time.sleep(0.01)
        else:
            time.sleep(0.001)

if __name__ == '__main__':
    main()