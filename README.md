# SAND SIMULATOR

## Description

The **Sand simulator**, aka a falling sand automaton, is a **cellular automaton** that simulates how grains of sand move due to gravity and create piles. It have simple rules:

**Gravity rule**: If the cell below a sand grain is empty, the sand grain moves to the empty cell.

**Collision rule**: If the cell below a sand grain is full but the cell at bottom left or the cell at bottom right is free, the sand grain moves there. If both are free, choose one randomly.

**Stay rule**: In the other cases, the sand grain doesn't move.

## Ambitions / Roadmap

<span style="color:green">***V0.1.0***</span>: Gravity rule (but reverse, i.e grains rise instead of falling)

<span style="color:green">***V0.1.1***</span>: Gravity rule

<span style="color:green">***V0.1.2***</span>: Collision rule

<span style="color:green">***V0.1.3***</span>: fix the borders, i.e, the window is like a sand box. So when a grain of sand falls onto the top of the pile and is on a border on one side, the grain falls (with the collision rule) on the other side.
